/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.validator;

public interface Validator<T> {

    /**
     * Method should validate given object
     *
     * @throws RuntimeException when given object is invalid
     */
    void validate(T object);
}
