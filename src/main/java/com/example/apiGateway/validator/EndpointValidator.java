/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.validator;

import com.example.apiGateway.parser.Endpoint;
import com.example.apiGateway.validator.Validator;
import org.springframework.stereotype.Component;

@Component
public class EndpointValidator implements Validator<Endpoint> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(Endpoint object) {
        if (isNotSetRequiredFields(object)) {
            throw new RuntimeException("Missing @url@ or @method@ fields " +
                    "in endpoint with id " + object.getId());
        }
    }

    private boolean isNotSetRequiredFields(Endpoint endpointDto) {
        return endpointDto.getMethod() == null
                || endpointDto.getUrlPattern() == null
                || endpointDto.getUrlPattern().trim().isEmpty();
    }
}
