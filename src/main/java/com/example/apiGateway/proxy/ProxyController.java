/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.proxy;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.HEAD;
import static org.springframework.web.bind.annotation.RequestMethod.OPTIONS;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Api gateway controller for proxy all requests from this
 * controller to special microservice
 */
@RestController
public class ProxyController {

    /**
     * Handler for requests with payload
     */
    private final ProxyHandler withPayloadHandler;

    /**
     * @param withPayloadHandler Handler for requests with payload
     */
    public ProxyController(ProxyHandler withPayloadHandler) {
        this.withPayloadHandler = withPayloadHandler;
    }

    /**
     * This method should proxy all request which can not have request
     * body, for example {@code get, head, ...}
     *
     * @param request Servlet request
     * @return Response from microservice
     */
    @RequestMapping(value = "/**", method = {GET, HEAD, OPTIONS})
    public Object doProxyForMethodsWithoutBody(HttpServletRequest request) {
        return withPayloadHandler.doProxy(request);
    }

    /**
     * This method should proxy all request which can have request body
     *
     * @param req    Servlet request
     * @param body   Request body
     * @return Response from microservice
     */
    @RequestMapping(value = "/**", method = {POST, PUT, PATCH, DELETE})
    public ResponseEntity<?> doProxyForMethodsWithBody(
            HttpServletRequest req, @RequestBody Object body) {

        return withPayloadHandler.doProxy(req, body);
    }
}
