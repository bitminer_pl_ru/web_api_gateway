/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.proxy;

import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Handler which should proxy given request to special microservice
 */
public interface ProxyHandler {

    /**
     * This method should proxy given request
     *
     * @param request Servlet request
     * @param args    Request arguments
     * @return Response
     */
    ResponseEntity<Object> doProxy(HttpServletRequest request, Object... args);
}
