/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.proxy;

import com.example.apiGateway.parser.Endpoint;
import com.example.apiGateway.parser.Microservice;
import com.example.apiGateway.registry.EndpointsRegistry;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * This handler proxy requests with payload, for example
 * {@code post, put, patch}
 *
 * todo Отрефакторить обязанности класса, сейчас он много
 * чего умеет
 */
@Slf4j
@Component
public class ProxyWithPayloadHandler implements ProxyHandler {

    /**
     * Spring rest template for execute requests
     */
    private final RestTemplate restTemplate;

    /**
     * Registry for proxy
     */
    private final EndpointsRegistry registry;

    /**
     * Headers converter
     */
    private final Converter<HttpServletRequest, HttpHeaders> headersConverter;

    /**
     * @param restTemplate     Template for execute rest queries
     * @param registry         Registry with endpoints for proxy
     * @param headersConverter Http Servlet Request headers to spring
     *                         headers converter
     */
    public ProxyWithPayloadHandler(@NonNull RestTemplate restTemplate,
                                   @NonNull EndpointsRegistry registry,
                                   @NonNull Converter<HttpServletRequest, HttpHeaders> headersConverter) {
        this.restTemplate = restTemplate;
        this.registry = registry;
        this.headersConverter = headersConverter;
    }

    /**
     * {@inheritDoc}
     * <p>
     * This class may work with any request types
     */
    @Override
    public ResponseEntity<Object> doProxy(@NonNull HttpServletRequest request,
                                          @NonNull Object... args) {
        String givenPath = request.getServletPath();
        String givenMethod = request.getMethod();

        Endpoint endpoint = obtainEndpoint(givenPath, givenMethod);
        String url = buildUrlToMicroservice(endpoint.getService(), request);

        HttpEntity<Object> payload = createHttpEntity(headersConverter.convert(request), args);
        return restTemplate.exchange(url, endpoint.getMethod(), payload, Object.class);
    }

    private String buildUrlToMicroservice(@NonNull Microservice microservice,
                                          @NonNull HttpServletRequest request) {
        String url = microservice.getBaseUrl() + request.getServletPath();
        String queryString;
        if ((queryString = request.getQueryString()) != null) {
            url += "?" + queryString;
        }
        return url;
    }

    private HttpEntity<Object> createHttpEntity(@NonNull HttpHeaders headers,
                                                @NonNull Object... args) {
        // If given request has payload
        return (args.length > 0)
                ? new HttpEntity<>(args[0], headers)
                : new HttpEntity<>(headers);
    }

    private Endpoint obtainEndpoint(String url, String method) {
        return registry.getEndpointByUrlAndMethod(url, method)
                .orElseThrow(() -> new EndpointNotFoundException(url));
    }
}
