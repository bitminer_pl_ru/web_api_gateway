/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.proxy;

import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * Should obtain http headers from srevlet request and convert them
 * to {@link HttpHeaders} map
 */
@Component
public class HttpServletRequestToHttpHeadersConverter
        implements Converter<HttpServletRequest, HttpHeaders> {

    /**
     * {@inheritDoc}
     */
    @Override
    public HttpHeaders convert(HttpServletRequest req) {
        HttpHeaders headers = new HttpHeaders();

        Enumeration<String> headerNames = req.getHeaderNames();

        for (; headerNames.hasMoreElements(); ) {
            String header = headerNames.nextElement();

            headers.add(header, req.getHeader(header));
        }

        return headers;
    }
}
