/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway;

import com.example.apiGateway.parser.ServiceFileParser;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Listener for event which fire when application was
 * be started, it need for register micro services in
 * gateway
 */
@Component
public class ApplicationStartedListener {
    private final ServiceFileParser serviceFileParser;
    private final GatewayConfiguration config;

    public ApplicationStartedListener(ServiceFileParser serviceFileParser,
                                      GatewayConfiguration config) {
        this.serviceFileParser = serviceFileParser;
        this.config = config;
    }

    /**
     * Method should call micro services registar for registartion
     * services in system
     *
     * todo process exceptions normally!
     */
    @EventListener(classes = ContextRefreshedEvent.class)
    public void onApplicationStarted() {
        try (InputStream inputStream = new FileInputStream(config.getServicesFile())) {
            serviceFileParser.registerServices(inputStream);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("File " + config.getServicesFile() + " not found!");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
