/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

/**
 * Error handler for handle errors from responses
 */
@Slf4j
public class RestTemplateErrorHandler implements ResponseErrorHandler {
    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return response.getRawStatusCode() >= 400;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        log.error("Call microservice error, given response {}", response);
    }
}
