/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * External configuration for api gateway server
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "gateway")
public class GatewayConfiguration {

    /**
     * File which contains services set, with base url
     * and registered endpoints for proxy and information
     * about authentication
     */
    private String servicesFile;
}
