/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.parser.exceptions;

public class NotFoundRequiredBlock extends RuntimeException {
    public NotFoundRequiredBlock(String block) {
        super("Required block @" + block + "@ not found in " +
                "services file");
    }
}
