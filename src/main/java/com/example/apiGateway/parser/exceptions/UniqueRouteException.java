/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.parser.exceptions;

import com.example.apiGateway.parser.Endpoint;

/**
 * Exception when exists not unique route in file
 */
public class UniqueRouteException extends RuntimeException {
    private static final String ERROR_TEMPLATE = "Endpoint node with id " +
            "%s contains conflict endpoint definition %s %s with node " +
            "with id %s";

    public UniqueRouteException(Endpoint endpoint, String conflictEndpointId) {
        super(String.format(ERROR_TEMPLATE,
                endpoint.getId(), endpoint.getMethod().toString(),
                endpoint.getUrlPattern(), conflictEndpointId));
    }
}
