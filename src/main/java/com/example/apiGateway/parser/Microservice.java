/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.parser;

import lombok.Data;

/**
 * Microservice dto, contains general information about
 * microservice
 */
@Data(staticConstructor = "of")
public class Microservice {

    /** Microservice id for logging */
    private final String id;

    /** Base url for proxy requests to service */
    private final String baseUrl;
}
