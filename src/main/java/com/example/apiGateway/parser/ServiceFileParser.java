/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.parser;

import com.example.apiGateway.registry.EndpointsRegistry;
import com.example.apiGateway.parser.exceptions.NotFoundRequiredBlock;
import com.example.apiGateway.validator.Validator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

/**
 * This class contains information about all micro services
 * in system and endpoints in that micro services
 * <p>
 * todo: refactor parser
 */
@Component
public class ServiceFileParser {

    /** Object mapper for parse yaml services pool file */
    private final ObjectMapper YAML_OBJECT_MAPPER = new ObjectMapper(new YAMLFactory());

    /** Registry for register endpoints */
    private final EndpointsRegistry registry;

    /** Validator for validate endpoint dto object */
    private final Validator<Endpoint> endpointValidator;

    public ServiceFileParser(EndpointsRegistry registry,
                             Validator<Endpoint> endpointValidator) {
        this.registry = registry;
        this.endpointValidator = endpointValidator;
    }

    /**
     * Method should register all endpoints from service file in
     * gateway
     *
     * @param ymlInputStream File input stream
     */
    public void registerServices(InputStream ymlInputStream) {
        try {
            unsafeServicesRegister(ymlInputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Unsafe parse all endpoints from file
     *
     * @param inputStream File input stream
     * @throws IOException When errors in file or we cannot read
     *                     file with services
     */
    private void unsafeServicesRegister(InputStream inputStream)
            throws IOException {
        JsonNode root = YAML_OBJECT_MAPPER.readTree(inputStream);

        registerEachService(obtainRequiredNode(root, "microservices"));
    }

    /**
     * Obtain required node from root node
     *
     * @param root Root node
     * @param node Required node
     * @return Obtained node
     */
    private JsonNode obtainRequiredNode(JsonNode root, String node) {
        return Optional.ofNullable(root.get(node))
                .orElseThrow(() -> new NotFoundRequiredBlock(node));
    }

    /**
     * Register each service in endpoint registry
     *
     * @param microservices Node with microservices
     */
    private void registerEachService(JsonNode microservices) {
        for (Iterator<Map.Entry<String, JsonNode>> it = microservices.fields();
             it.hasNext(); ) {
            Map.Entry<String, JsonNode> service = it.next();

            doRegisterMicroservice(service.getKey(), service.getValue());
        }
    }

    /**
     * Register open and secured endpoints in registry
     *
     * @param serviceId         microservice id
     * @param serviceDescriptor microservice descriptor
     */
    private void doRegisterMicroservice(String serviceId, JsonNode serviceDescriptor) {
        Microservice serviceDto = Microservice.of(serviceId,
                obtainRequiredNode(serviceDescriptor, "baseUrl").textValue());

        if (serviceDescriptor.has("openEndpoints")) {
            registerEndpoints(serviceDescriptor.get("openEndpoints"), false, serviceDto);
        }

        if (serviceDescriptor.has("securedEndpoints")) {
            registerEndpoints(serviceDescriptor.get("securedEndpoints"), true, serviceDto);
        }
    }

    /**
     * Register endpoints collection in registry
     *
     * @param endpointsNode            Endpoints node
     * @param isAuthenticatedEndpoints Is authentication required for
     *                                 endpoint {@code true} if yes,
     *                                 otherwise {@code false}
     * @param serviceOwner             Microservice which owning endpoints
     */
    private void registerEndpoints(JsonNode endpointsNode, boolean isAuthenticatedEndpoints,
                                   Microservice serviceOwner) {

        for (Iterator<Map.Entry<String, JsonNode>> openEndpointsIt = endpointsNode.fields();
             openEndpointsIt.hasNext(); ) {

            Map.Entry<String, JsonNode> endpoint = openEndpointsIt.next();

            Endpoint endpointDto = obtainEndpointFromJsonNode(
                    endpoint.getValue());
            endpointDto.setId(endpoint.getKey());
            endpointDto.setService(serviceOwner);
            endpointDto.setRequiredAuth(isAuthenticatedEndpoints);

            endpointValidator.validate(endpointDto);

            registry.register(endpointDto);
        }
    }

    private Endpoint obtainEndpointFromJsonNode(JsonNode node) {
        try {
            return YAML_OBJECT_MAPPER.treeToValue(
                    node, Endpoint.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
