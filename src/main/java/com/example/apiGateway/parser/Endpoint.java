/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.parser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.http.HttpMethod;

import javax.validation.constraints.NotNull;

import static java.util.Objects.requireNonNull;

/**
 * Microservice endpoint
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true, value = {"id", "isRequiredAuth", "service"})
public class Endpoint {

    /** Endpoint id, required for loging and monitoring */
    private String id;

    /** Http method */
    @NotNull
    private HttpMethod method;

    /** Pattern for search endpoint */
    @NotNull
    @JsonProperty("url")
    private String urlPattern;

    /** Required authentication token for this endpoint */
    private boolean isRequiredAuth;

    /** Service which contains this endpoint */
    private Microservice service;

    public Endpoint() {
    }

    public Endpoint(String id, HttpMethod method, String urlPattern) {
        this(id, method, urlPattern, null);
    }

    public Endpoint(String id, HttpMethod method, String urlPattern, Microservice service) {
        this.id = id;
        this.method = method;
        this.urlPattern = urlPattern;
        this.service = service;
    }
}
