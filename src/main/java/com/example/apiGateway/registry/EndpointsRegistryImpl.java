/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.registry;

import com.example.apiGateway.parser.Endpoint;
import com.example.apiGateway.parser.exceptions.UniqueRouteException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.WeakHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

/**
 * Registry implementation
 * <p>
 * todo tuning caches and change endpoints key
 */
@Component
public class EndpointsRegistryImpl implements EndpointsRegistry {
    private static final String SEPARATOR = ":";

    /**
     * Endpoints in memory db
     */
    private final Map<String, Endpoint> endpoints = new HashMap<>();

    /**
     * Cache for compiled patterns
     */
    private final Map<String, Pattern> patternsCache = new WeakHashMap<>();

    /**
     * Cache for splitting data
     */
    private final Map<String, String[]> splitCache = new WeakHashMap<>();

    /**
     * {@inheritDoc}
     * <p>
     * When exists duplicate routes in endpoints we will throw
     * {@link UniqueRouteException}, because there can be no
     * conflict of routes
     */
    @Override
    public void register(Endpoint endpoint) {
        String registerKey = createEndpointKey(endpoint.getUrlPattern(),
                endpoint.getMethod().name());

        if (endpoints.containsKey(registerKey)) {
            throw new UniqueRouteException(endpoint,
                    endpoints.get(registerKey).getId());
        }

        endpoints.put(registerKey, endpoint);
    }

    private String createEndpointKey(String urlPattern, String method) {
        return urlPattern + SEPARATOR + method.toLowerCase();
    }

    /**
     * {@inheritDoc}
     * <p>
     * This method should return endpoint from registry, this
     * method supports regular expressions in route definition,
     * so this method at first find by simple key and later by
     * evaluating regular expressions
     */
    @Override
    public Optional<Endpoint> getEndpointByUrlAndMethod(String urlPattern, String method) {
        Endpoint endpoint = Optional.ofNullable(obtainEndpointByKey(urlPattern, method))
                .orElseGet(() -> obtainByRegexp(urlPattern, method));

        return Optional.ofNullable(endpoint);
    }

    /**
     * This method find endpoint by simple key, for example
     * {@code /users SEPARATOR post}
     *
     * @param urlPattern url pattern for search in keys
     * @param method     http method
     * @return founded endpoint or {@code null} if endpoint
     * not found in registry
     */
    private Endpoint obtainEndpointByKey(String urlPattern, String method) {
        return endpoints.get(createEndpointKey(urlPattern, method));
    }

    /**
     * This is hard find by regexp evaluation, if endpoint
     * by key could not be found, we should attempt find
     * endpoint by regexp evaluation
     *
     * @param urlPattern Real pattern from request
     * @param method     http method
     * @return Founded endpoint or {@code null} if endpoint
     * not found in registry
     */
    private Endpoint obtainByRegexp(String urlPattern, String method) {
        for (String pattern : endpoints.keySet()) {
            String[] split = obtainSplittingByPattern(pattern);
            if (!split[1].equalsIgnoreCase(method)) {
                continue;
            }
            Matcher matcher = obtainPatternObjectByRegex(split[0])
                    .matcher(urlPattern);
            if (matcher.find())
                return endpoints.get(pattern);
        }
        return null;
    }

    /**
     * Cacheable regex split result
     *
     * @param pattern pattern for splitting
     * @return splitted data from cache or new split
     */
    private String[] obtainSplittingByPattern(String pattern) {
        return splitCache.computeIfAbsent(pattern,
                (key) -> key.split(SEPARATOR));
    }

    /**
     * Cached regex patterns after call {@link Pattern#compile(String)}
     *
     * @param regex Regex for compilation
     * @return compiled pattern
     */
    private Pattern obtainPatternObjectByRegex(String regex) {
        return patternsCache.computeIfAbsent(regex,
                key -> compile(regex + "/?$", CASE_INSENSITIVE));
    }
}
