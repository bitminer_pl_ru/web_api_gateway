/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway.registry;

import com.example.apiGateway.parser.Endpoint;

import java.util.Optional;

/**
 * Registry for proxy endpoints, contains information about
 * all endpoints for proxy
 */
public interface EndpointsRegistry {

    /**
     * Register new endpoint in registry
     *
     * @param endpoint Endpoint for registration, must be
     *                 {@code not null}
     */
    void register(Endpoint endpoint);

    /**
     * Return endpoint by method and urlPattern
     *
     * @param urlPattern Url pattern
     * @param method     http method
     * @return Endpoint class
     */
    Optional<Endpoint> getEndpointByUrlAndMethod(String urlPattern, String method);
}
