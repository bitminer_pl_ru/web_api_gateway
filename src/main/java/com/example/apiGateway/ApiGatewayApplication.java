/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Run api gateway service
 */
@SpringBootApplication
public class ApiGatewayApplication {

    /**
     * Rest template bean for create proxy requests to microservices
     *
     * @return Rest template service
     */
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new RestTemplateErrorHandler());

        return restTemplate;
    }

    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayApplication.class, args);
    }
}
