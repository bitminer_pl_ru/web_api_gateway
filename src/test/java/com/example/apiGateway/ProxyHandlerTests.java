/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway;

import com.example.apiGateway.parser.Endpoint;
import com.example.apiGateway.parser.Microservice;
import com.example.apiGateway.proxy.EndpointNotFoundException;
import com.example.apiGateway.proxy.HttpServletRequestToHttpHeadersConverter;
import com.example.apiGateway.proxy.ProxyHandler;
import com.example.apiGateway.proxy.ProxyWithPayloadHandler;
import com.example.apiGateway.registry.EndpointsRegistry;
import com.example.apiGateway.registry.EndpointsRegistryImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ProxyHandlerTests {
    private static final String ALL_HEADERS_VALUE = "abc";

    /**
     * General response entity for all requests
     */
    private ResponseEntity<Object> responseEntity = ResponseEntity.ok()
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-Powered", "test")
            .body("success");

    /**
     * Headers for all requests, we will check header values after proxy
     */
    private Map<String, List<String>> headers = new HashMap<String, List<String>>() {{
        put("Content-Type", singletonList(ALL_HEADERS_VALUE));
        put("X-Token", singletonList(ALL_HEADERS_VALUE));
    }};

    /**
     * Mocked rest template
     */
    @Mock
    private RestTemplate restTemplate;

    /**
     * Request mock
     */
    @Mock
    private HttpServletRequest request;

    /**
     * Testing object instance
     */
    private ProxyHandler proxyHandler;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        initRestTemplateMockCalls();
        initDefaultRequestMethodsInvocation();

        this.proxyHandler = createProxyHandlerInstance();
    }

    /**
     * When given just {@code GET} request to special
     * microservice. Proxy should send request and return
     * response from service
     */
    @Test
    public void whenGivenSimpleGetRequest() {
        when(request.getServletPath()).thenReturn("/accounts");
        when(request.getMethod()).thenReturn("GET");

        ResponseEntity<Object> responseEntity = proxyHandler.doProxy(request);

        assertEquals(this.responseEntity, responseEntity);
    }

    /**
     * Case when given request path not found in registry, proxy
     * handler should throw {@link EndpointNotFoundException}
     */
    @Test(expected = EndpointNotFoundException.class)
    public void whenGivenEndpointNotFoundInRegistry() {
        when(request.getServletPath()).thenReturn("/users");
        when(request.getMethod()).thenReturn("POST");

        proxyHandler.doProxy(request);
    }

    /**
     * When given request with payload, proxy should send that
     * payload to microservice for correct work microservices
     */
    @Test
    public void whenGivenRequestWithPayload() {
        when(request.getServletPath()).thenReturn("/accounts");
        when(request.getMethod()).thenReturn("POST");

        Map<String, String> payload = singletonMap("username", "w.gusser");

        ResponseEntity<?> responseEntity = proxyHandler.doProxy(request, payload);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.putAll(headers);

        HttpEntity<Object> entity = new HttpEntity<>(payload, httpHeaders);

        // Is was called method with given payload and headers
        verify(restTemplate).exchange(anyString(), eq(HttpMethod.POST),
                eq(entity), eq(Object.class));

        assertEquals(this.responseEntity, responseEntity);
    }

    /**
     * Init rest template mock methods invocation
     */
    private void initRestTemplateMockCalls() {
        ResponseEntity<Object> exchange = restTemplate.exchange(
                anyString(), any(HttpMethod.class), any(), eq(Object.class));

        when(exchange).thenReturn(responseEntity);
    }

    private void initDefaultRequestMethodsInvocation() {
        when(request.getHeaderNames()).thenReturn(createHeaderNamesEnumeration());
        when(request.getHeader(anyString())).thenReturn(ALL_HEADERS_VALUE);
    }

    /**
     * Create stub headers for servlet requests
     *
     * @return Header names enumeration
     */
    private Enumeration<String> createHeaderNamesEnumeration() {
        return new Vector<>(this.headers.keySet()).elements();
    }

    /**
     * Create new proxy handler instance, this object will be testing
     *
     * @return Proxy handler instance
     */
    private ProxyHandler createProxyHandlerInstance() {
        /* Converter for headers */
        Converter<HttpServletRequest, HttpHeaders> converter = new HttpServletRequestToHttpHeadersConverter();

        return new ProxyWithPayloadHandler(restTemplate, createEndpointsRegistry(), converter);
    }

    /**
     * Create new endpoints registry instance with stub data for
     * testing proxy handler
     *
     * @return Endpoints registry with endpoints
     */
    private EndpointsRegistry createEndpointsRegistry() {
        Microservice service = Microservice.of("example", "http://localhost:3001");

        EndpointsRegistry registry = new EndpointsRegistryImpl();
        registry.register(new Endpoint("example", HttpMethod.GET, "/accounts", service));
        registry.register(new Endpoint("example2", HttpMethod.POST, "/accounts", service));

        return registry;
    }
}
