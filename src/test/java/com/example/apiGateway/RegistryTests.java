/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway;

import com.example.apiGateway.parser.Endpoint;
import com.example.apiGateway.parser.exceptions.UniqueRouteException;
import com.example.apiGateway.registry.EndpointsRegistry;
import com.example.apiGateway.registry.EndpointsRegistryImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpMethod;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Endpoints registry tests
 */
public class RegistryTests {

    /**
     * Endpoints registry instance
     */
    private EndpointsRegistry registry;

    /**
     * Create new registry before each test
     */
    @Before
    public void beforeTest() {
        registry = new EndpointsRegistryImpl();
    }

    /**
     * When all data is correct registry should work normally.
     */
    @Test
    public void whenAllDataIsCorrect() {
        registry.register(new Endpoint("example", HttpMethod.GET, "/users/{id}"));
        registry.register(new Endpoint("example2", HttpMethod.PUT, "/users/{id}"));

        assertTrue(registry.getEndpointByUrlAndMethod("/users/{id}", "GET").isPresent());
        assertTrue(registry.getEndpointByUrlAndMethod("/users/{id}", "PUT").isPresent());
    }

    /**
     * When system attempt register duplicate route, registry should throw
     * {@link UniqueRouteException}
     */
    @Test(expected = UniqueRouteException.class)
    public void whenExistsDuplicateRoutes() {
        Endpoint[] endpoints = {
                new Endpoint("example", HttpMethod.GET, "/users/{id}"),
                new Endpoint("example2", HttpMethod.GET, "/users/{id}")
        };

        for (Endpoint endp : endpoints)
            registry.register(endp);
    }

    /**
     * Registry support regexp for search endpoints, this test check
     * regexp patterns in service file
     */
    @Test
    public void whenGivenRegexpAsTemplate() {
        // register regexp template
        registry.register(new Endpoint("example", HttpMethod.GET, "/users/[0-9]+"));

        assertTrue(registry.getEndpointByUrlAndMethod("/users/21", "GET").isPresent());
        assertFalse(registry.getEndpointByUrlAndMethod("/users/21", "PUT").isPresent());
    }

    /**
     * Bug. When we did not find the route by a simple key, we try to
     * find it by the regular expression, and here we all routes begin
     * to coincide with weak regular expressions
     */
    @Test
    public void whenGivenRegexAndSimpleTemplate() {
        registry.register(new Endpoint("example", HttpMethod.POST, "/users"));

        assertFalse(registry.getEndpointByUrlAndMethod("/users/abc", "POST").isPresent());
    }

    /**
     * This case covers the situation when the router ends with a slash,
     * the same routes but one ends with a slash, and the second one is
     * not, should be treated as one route
     */
    @Test
    public void whenGivenPathEndsWithSlash() {
        registry.register(new Endpoint("example", HttpMethod.GET, "/users"));

        assertTrue(registry.getEndpointByUrlAndMethod("/users", "GET").isPresent());
        assertTrue(registry.getEndpointByUrlAndMethod("/users/", "GET").isPresent());
    }
}
