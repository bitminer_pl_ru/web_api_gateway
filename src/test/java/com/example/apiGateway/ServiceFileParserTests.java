/*
 * This code belongs to the company BitMainer, decompilation, research and use for its own 
 * purposes is prohibited, it is legally prosecuted
 *
 * Copyright (c) 2017
 */

package com.example.apiGateway;

import com.example.apiGateway.registry.EndpointsRegistry;
import com.example.apiGateway.registry.EndpointsRegistryImpl;
import com.example.apiGateway.validator.EndpointValidator;
import com.example.apiGateway.parser.ServiceFileParser;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Test all proxy methods in proxy controller, it should
 * proxy requests to endpoints pool in gateway
 */
public class ServiceFileParserTests {

    private static EndpointsRegistry ENDPOINTS_REGISTRY;
    private static ServiceFileParser SERVICE_FILE_PARSER;

    /**
     * Setup environment
     */
    @BeforeClass
    public static void setUpSuite() {
        ENDPOINTS_REGISTRY = new EndpointsRegistryImpl();
        SERVICE_FILE_PARSER = new ServiceFileParser(
                ENDPOINTS_REGISTRY, new EndpointValidator());
    }

    /**
     * Check proxy controller. It should proxy all request to
     * special micro services from configuration pool
     */
    @Test
    public void whenServiceFileIsCorrect() throws IOException {
        Resource servicesPool = new ClassPathResource("test-services.yml");
        try (InputStream input = servicesPool.getInputStream()) {
            SERVICE_FILE_PARSER.registerServices(input);
        }
        assertNotNull("Endpoint registry doesn't have POST /users endpoint",
                ENDPOINTS_REGISTRY.getEndpointByUrlAndMethod("/users", "POST"));
    }

    /**
     * Check case when not found required {@code microservices}
     * block in services file
     */
    @Test
    public void whenGivenIncorrectServiceFiles() throws IOException {
        String[][] incorrectFiles = {
                {"test-services-incorrect1.yml", "Required block @microservices@ not found in services file"},
                {"test-services-incorrect2.yml", "Required block @baseUrl@ not found in services file"},
                {"test-services-incorrect3.yml", "Missing @url@ or @method@ fields in endpoint with id root"},
                {"test-services-incorrect4.yml", "Missing @url@ or @method@ fields in endpoint with id root"}
        };

        for (String[] fileSettings : incorrectFiles) {
            evaluateServicesFile(fileSettings[0], fileSettings[1]);
        }
    }

    private void evaluateServicesFile(String fileName, String errorMess) throws IOException {
        Resource incorrectFile = new ClassPathResource(fileName);

        try (InputStream inputStream = incorrectFile.getInputStream()) {
            SERVICE_FILE_PARSER.registerServices(inputStream);

            fail("File " + fileName + " must be is incorrect, but it is correct");
        } catch (RuntimeException e) {
            assertEquals(errorMess, e.getMessage());
        }
    }
}
